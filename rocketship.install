<?php

/**
 * @file
 * Install file for rocketship.
 */

/**
 * Implements hook_install().
 */
function rocketship_install() {

  // Create the issue content type.
  $type = array(
    'type' => 'rocketship_issue',
    'name' => st('Rocketship issue'),
    'description' => st('Represents issues pulled from drupal.org.'),
    'base' => 'node_content',
    'custom' => 1,
    'modified' => 1,
    'locked' => 0,
  );
  $type = node_type_set_defaults($type);
  node_type_save($type);
  node_add_body_field($type);

  variable_set('rocketship_node_type', 'rocketship_issue');

  // Default variables for the  issue content type.
  variable_set('node_preview_rocketship_issue', 0);
  variable_set('node_submitted_rocketship_issue', 0);
  variable_set('menu_options_rocketship_issue', array());
  variable_set('node_options_rocketship_issue', array('status'));
  variable_set('comment_rocketship_issue', COMMENT_NODE_HIDDEN);

  // Create a vocabulary for tags.
  $description = st('Use tags to group issues on similar topics into categories.');
  $help = st('Enter a comma-separated list of words to describe the issue.');
  $issue_tags_vocabulary = (object) array(
    'name' => st('Rocketship issue tags'),
    'description' => $description,
    'machine_name' => 'rocketship_tags',
    'help' => $help,

  );
  taxonomy_vocabulary_save($issue_tags_vocabulary);

  // Save the vocabulary id.
  variable_set('rocketship_issue_tags_vid', $issue_tags_vocabulary->vid);

  $field = array(
    'field_name' => 'field_rocketship_issue_tags',
    'type' => 'taxonomy_term_reference',
    // Set cardinality to unlimited for tagging.
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $issue_tags_vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_rocketship_issue_tags',
    'entity_type' => 'node',
    'label' => 'Rocketship issue tags',
    'bundle' => 'rocketship_issue',
    'description' => $issue_tags_vocabulary->help,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  field_create_instance($instance);
  variable_set('rocketship_issue_tags_field', 'field_rocketship_issue_tags');

  // Create a textfield named "Assigned".
  $field = array(
    'field_name' => 'field_rocketship_issue_assigned',
    'type' => 'text',
    'cardinality' => 1,
    'settings' => array()
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_rocketship_issue_assigned',
    'entity_type' => 'node',
    'label' => 'Assigned',
    'bundle' => 'rocketship_issue',
    'description' => st('The drupal.org username to whom this issue is assigned.'),
    'widget' => array(
      'type' => 'text_textfield',
      'weight' => -3,
    ),
    'display' => array(
      'default' => array(
        'type' => 'text_default',
        'label' => 'inline',
        'weight' => 9,
      ),
      'teaser' => array(
        'type' => 'text_default',
        'label' => 'inline',
        'weight' => 9,
      ),
    ),
  );
  field_create_instance($instance);
  variable_set('rocketship_issue_assigned_field', 'field_rocketship_issue_assigned');

  // Create the rocketship page content type.
  $rocketship_page_type = array(
    'type' => 'rocketship_page',
    'name' => st('Rocketship page'),
    'description' => st('Allows creating pages with issue boards.'),
    'base' => 'node_content',
    'custom' => 1,
    'modified' => 1,
    'locked' => 0,
  );
  $rocketship_page_type = node_type_set_defaults($rocketship_page_type);
  node_type_save($rocketship_page_type);

  // Default variables for the initiative pages content type.
  variable_set('node_preview_rocketship_page', 0);
  variable_set('node_submitted_rocketship_page', 0);
  variable_set('menu_options_rocketship_page', array());
  variable_set('node_options_rocketship_page', array('status'));
  variable_set('comment_rocketship_page', COMMENT_NODE_HIDDEN);

  // Issue display tag for the rocketship page.
  $field = array(
    'field_name' => 'field_rocketship_page_tag',
    'type' => 'taxonomy_term_reference',
    'cardinality' => 1,
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $issue_tags_vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_rocketship_page_tag',
    'entity_type' => 'node',
    'label' => 'Rocketship page tag',
    'bundle' => 'rocketship_page',
    'description' => st('Select one tag to display issues from.'),
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -3,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  field_create_instance($instance);
  variable_set('rocketship_page_tag_field', 'field_rocketship_page_tag');

  // Create initiative page body field. We do this here, so it
  // appears at the bottom of the node form.
  node_add_body_field($rocketship_page_type);
}
